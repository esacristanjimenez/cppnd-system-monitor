#include <unistd.h>
#include <cctype>
#include <sstream>
#include <string>
#include <vector>

#include "process.h"
#include "linux_parser.h"
 
using std::string;
using std::to_string;
using std::vector;

Process::Process(int pid):pid_nr(pid){}

// TODO: Return this process's ID
// OK
int Process::Pid() {
    return pid_nr;
}

// TODO: Return this process's CPU utilization
// OK
float Process::CpuUtilization() const {
    float time = float(LinuxParser::UpTime() - LinuxParser::UpTime(pid_nr));
    return float(LinuxParser::ActiveJiffies(pid_nr)) / time;
}

// TODO: Return the command that generated this process
// OK
string Process::Command() {
  	if(LinuxParser::Command(pid_nr).size()>50){
        return LinuxParser::Command(pid_nr).substr(0,50);
    }else{
        return LinuxParser::Command(pid_nr);
    }
}

// TODO: Return this process's memory utilization
// OK
string Process::Ram() {
    return LinuxParser::Ram(pid_nr);
}

// TODO: Return the user (name) that generated this process
// OK
string Process::User() {
    return LinuxParser::User(pid_nr);
}

// TODO: Return the age of this process (in seconds)
// OK
long int Process::UpTime() {
    return LinuxParser::UpTime();
}

// TODO: Overload the "less than" comparison operator for Process objects
// REMOVE: [[maybe_unused]] once you define the function
// OK
bool Process::operator<(Process const& a) const {
    return a.CpuUtilization() < this->CpuUtilization();
}