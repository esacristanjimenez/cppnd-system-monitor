#include "processor.h"
#include "linux_parser.h"
#include <unistd.h>

float Processor::Utilization() {
    int deltaTotal=0, deltaIdle=0;
    currIdle = LinuxParser::IdleJiffies();
    currTotal = LinuxParser::Jiffies();
    
    deltaTotal = currTotal - prevTotal;
    deltaIdle = currIdle - prevIdle;
    
    prevIdle=currIdle;
    prevTotal=currTotal;
    
    return 1.0 * (deltaTotal - deltaIdle) / deltaTotal;
}