#include <string>
#include <iostream>
#include <cmath>

#include "format.h"

using std::string;

string Format::ElapsedTime(long seconds) {
    long h, m, s;
    string hora, mins, secs;

    h = seconds / 3600;
    m = (seconds % 3600) / 60;
    s = (seconds % 60);

    //Convert number to string and add double digits
    hora = std::to_string(h);

    if(m<10){ mins = '0' + std::to_string(m); }
    else{ mins =std::to_string(m); }

    if(s<10){ secs = '0' + std::to_string(s); }
    else{ secs =std::to_string(s); }

    return hora + ":" + mins + ":" + secs;
}