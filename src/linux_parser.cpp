#include <dirent.h>
#include <unistd.h>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>

#include "linux_parser.h"

using std::stof;
using std::stol;
using std::stoi;
using std::string;
using std::to_string;
using std::vector;


string LinuxParser::OperatingSystem() {
  string line;
  string key;
  string value;
  std::ifstream filestream(kOSPath);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::replace(line.begin(), line.end(), ' ', '_');
      std::replace(line.begin(), line.end(), '=', ' ');
      std::replace(line.begin(), line.end(), '"', ' ');
      std::istringstream linestream(line);
      while (linestream >> key >> value) {
        if (key == "PRETTY_NAME") {
          std::replace(value.begin(), value.end(), '_', ' ');
          return value;
        }
      }
    }
  }
  return value;
}

string LinuxParser::Kernel() {
  string os, kernel, version;
  string line;
  std::ifstream stream(kProcDirectory + kVersionFilename);
  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    linestream >> os >> version >> kernel;
  }
  return kernel;
}

vector<int> LinuxParser::Pids() {
  vector<int> pids;
  DIR* directory = opendir(kProcDirectory.c_str());
  struct dirent* file;
  while ((file = readdir(directory)) != nullptr) {
    // Is this a directory?
    if (file->d_type == DT_DIR) {
      // Is every character of the name a digit?
      string filename(file->d_name);
      if (std::all_of(filename.begin(), filename.end(), isdigit)) {
        int pid = stoi(filename);
        pids.push_back(pid);
      }
    }
  }
  closedir(directory);
  return pids;
}

float LinuxParser::MemoryUtilization() {
  string line, variable;
  int value = 0, run = 0;
  float memTotal = 0, memFree = 0;

  std::ifstream filestream(kProcDirectory+kMeminfoFilename);
  if(filestream.is_open()){
    while(std::getline(filestream, line)){
      std::istringstream linestream(line);
      while(linestream >> variable >> value){
        if(run<2){
          if(variable==filterMemTotalString){
            memTotal = value;
            run+=1;
          }else if(variable==filterMemFreeString){
            memFree = value;
            run+=1;
          }
        }
      }
    }
  }
  return (memTotal - memFree) / memTotal;
}

long LinuxParser::UpTime() {
  long uptime;
  string line, time;

  std::ifstream filestream(kProcDirectory + kUptimeFilename);
  if(filestream.is_open()){
    std::getline(filestream, line);
    std::istringstream linestream(line);
    linestream >> time;
    try{
      uptime=stol(time);
    }catch(...){
      uptime=0;
    }
  }
  return uptime;
}

long LinuxParser::Jiffies() {
  return ActiveJiffies() + IdleJiffies();
}

long LinuxParser::ActiveJiffies(int pid) {
  long activeJ=0;
  string line, variable;

  std::ifstream filestream(kProcDirectory + to_string(pid) + kStatFilename);
  if(filestream.is_open()){
    std::getline(filestream, line);
    std::istringstream linestream(line);
    //Save field (utime)
    for(int i=0; i<14; i++){
      linestream >> variable;
    }
    activeJ = stol(variable);

    //Save fields (stime, cutime, cstime)
    for(int i=0; i<3; i++){
      linestream >> variable;
      activeJ += stol(variable);
    }
  }
  return activeJ / sysconf(_SC_CLK_TCK);
}

long LinuxParser::ActiveJiffies() {
  //Solution from my questions in knowledge.udacity 
  auto jiffies = LinuxParser::CpuUtilization();
  std::vector<long> vector_long;
  std::transform(jiffies.begin(), jiffies.end(), std::back_inserter(vector_long), [](const string& s){return std::stol(s);});

  long sum = vector_long[kUser_] + vector_long[kNice_] + vector_long[kSystem_] + vector_long[kIRQ_] + vector_long[kSoftIRQ_] + vector_long[kSteal_];
  return sum;
}

long LinuxParser::IdleJiffies() {
  long idle=0, iowait=0;
  string line, variable;

  std::ifstream filestream(kProcDirectory + kStatFilename);
  if(filestream.is_open()){
    std::getline(filestream, line);
    std::istringstream linestream(line);
    //Save field (idle)
    for(int i=0; i<4; i++){
      linestream >> variable;
    }
    idle = stol(variable);
    //Save field (iowait)
    linestream >> variable;
    iowait = stol(variable);
  }
  return idle + iowait;
}

vector<string> LinuxParser::CpuUtilization() {
  //Solution from my questions in knowledge.udacity 
  vector<string> cpuStats;
  string line, variable, value;
  std::ifstream filestream(kProcDirectory + kStatFilename);
  if(filestream.is_open()){
    if(std::getline(filestream, line)){
      std::istringstream linestream(line);
      linestream >> variable;
      while(linestream >> value && cpuStats.size()<CPUStates::kGuest_){
        cpuStats.emplace_back(value);
      }
    }
  }
  return cpuStats;
}

int LinuxParser::TotalProcesses() {
  string line, variable;
  int value;
  
  std::ifstream filestream(kProcDirectory + kStatFilename);
  if(filestream.is_open()){
    while(std::getline(filestream, line)){
      std::istringstream linestream(line);
      while(linestream >> variable >> value){
        if(variable==filterProcesses){
          return value;
        }
      }
    }
  }
  return 0;
}

int LinuxParser::RunningProcesses() {
  string line, variable;
  int value;
  
  std::ifstream filestream(kProcDirectory + kStatFilename);
  if(filestream.is_open()){
    while(std::getline(filestream, line)){
      std::istringstream linestream(line);
      while(linestream >> variable >> value){
        if(variable==filterRunningProcesses){
          return value;
        }
      }
    }
  }
  return 0;
}

string LinuxParser::Command(int pid) {
  string line;

  std::ifstream filestream(kProcDirectory + to_string(pid) + kCmdlineFilename);
  if(filestream.is_open()){
    std::getline(filestream, line);
  }

  return line;
}

string LinuxParser::Ram(int pid) {
  string line, variable, value;

  std::ifstream filestream(kProcDirectory + to_string(pid) + kStatusFilename);
  if(filestream.is_open()){
    while(std::getline(filestream, line)){
      std::istringstream linestream(line);
      while(linestream >> variable >> value){
        if(variable==filterProcMem){
          return to_string(stol(value) / 1024);
        }
      }
    }
  }
  return value;
}

string LinuxParser::Uid(int pid) {
  string line, name, id;

  std::ifstream filestream(kProcDirectory + to_string(pid) + kStatusFilename);
  if(filestream.is_open()){
    while(std::getline(filestream, line)){
      std::istringstream linestream(line);
      while(linestream >> name >> id){
        if(name==filterUID){
          return id;
        }
      }
    }
  }
  return id;
}

string LinuxParser::User(int pid) {
  string line, user, id, x;

  string uid = Uid(pid);
  std::ifstream filestream(kPasswordPath);
  if(filestream.is_open()){
    while(std::getline(filestream, line)){
      std::replace(line.begin(), line.end(), ':', ' ');
      std::istringstream linestream(line);
      linestream >> user >> x >> id;
      if(id == uid){
        return user;
      }
    }
  }
  return " ";
}

long LinuxParser::UpTime(int pid) {
  string line, variable;

  std::ifstream filestream(kProcDirectory + to_string(pid) + kStatFilename);
  if(filestream.is_open()){
    while(std::getline(filestream, line)){
      std::istringstream linestream(line);
      for(int i=0; i<22; i++)
        linestream >> variable;
    }
    return stol(variable) / sysconf(_SC_CLK_TCK);
  }
  return 0;
}
